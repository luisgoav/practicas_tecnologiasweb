<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN” “http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Funciones</title>
            </head>
        <body>
            <p>---------------------------------1---------------------------------</p>
 <p>¿El numero es multiplo de 5 o 7?
<?php  
$n1 = $_GET["numero"];
if(( $n1 % 7 )  == 0 && ( $n1 % 5 ) == 0){

	echo $n1 . ' es multiplo de 5 y 7';
    
}else{

	echo $n1 . ' no es multiplo de 5 y 7';
}
?>
</p>
<br>
<p>---------------------------------2---------------------------------</p>
<p>Secuencia de números aleatorios</p>
<br>
<?php

$matriz = [[],[],[]];
$contador = 0;

do {
	global  $matriz;
	$pos1 = (int)rand(10,100);
	$pos2 = (int)rand(10,100);
	$pos3 = (int)rand(10,100);
	$matriz[0][] = $pos1;
	$matriz[1][] = $pos2;
	$matriz[2][] = $pos3;

	$contador ++;
}while($pos1 % 2 ==0 ||	 $pos2%2 !=0 || $pos3%2 == 0);

for($i=0; $i<$contador; $i++){
foreach($matriz as $lista )
{
	echo "$lista[$i],";
}
echo '<br>';
}
echo '<br>';
$num = $contador*3;
echo "$num numeros obtenidos de $contador iteraciones";
?>
<p>---------------------------------3---------------------------------</p>
<p>Variante usando while</p>
<?php
$num = $_GET["numero2"];
echo "El numero ingresado es $num <br>";

echo "Buscando el primer numero aleatorio que sea multiplo de $num<br>";

while(is_int($i=rand(10,100)) && $i % $num != 0){
   echo "Numero aleatorio: $i <br>";
}
echo "<br>Primer numero mulriplo de $num: $i";
?>
<br>
<p>Variante usando do while</p>
<?php
do{
    $j = rand(10,100);
    echo "Numero aleatorio: $j<br>";
}while(is_int($j) && $j % $num != 0);
echo "<br>Primer numero mulriplo de $num: $j";
?>
<p>---------------------------------4---------------------------------</p>
<?php
for(is_int($i = 97); $i <= 122; $i++)
{
    $letras = array($i,chr($i));
    foreach($letras as $numero => $letra)
    {
        echo "[$letra]  ";
    }
    echo "<br>";
}
?>
</body>
</html>